<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Matthew
 * Date: 13/11/13
 * Time: 16:14
 * To change this template use File | Settings | File Templates.
 */

// stop php complaining
date_default_timezone_set('UTC');

$dir = 'D:\Content\_incoming\website\Italian Verbs\italian.about.com\library\verbfixed';
$newdir = 'D:\Content\_incoming\website\Italian Verbs\italian.about.com\library\verbparsed';
$files = array();

// get the files from the dir
if (is_dir($dir)) {
    if ($dh = opendir($dir)) {
        while (($file = readdir($dh)) !== false) {
            if (substr($file,0,7) == "blverb_" ) {
                $files[] = $file;
            }
        }
        closedir($dh);
    }
}

function pronouns($tense) {

    echo $tense."_io".chr(9);
    echo $tense."_tu".chr(9);
    echo $tense."_lei".chr(9);
    echo $tense."_noi".chr(9);
    echo $tense."_voi".chr(9);
    echo $tense."_loro";
}

// print the headings
echo "Infinito";
echo chr(9);
echo "English";
echo chr(9);
echo "Reg";
echo chr(9);
echo "Type";
echo chr(9);

// indicative

pronouns("iPresente");
echo chr(9);
pronouns("iImperfetto");
echo chr(9);
pronouns("iPassatoRemoto");
echo chr(9);
pronouns("iFuturoSemplice");
echo chr(9);
pronouns("iPassatoProssimo");
echo chr(9);
pronouns("iTrapassatoProssimo");
echo chr(9);
pronouns("iTrapassatoRemoto");
echo chr(9);
pronouns("iFutureAnteriore");
echo chr(9);

// subjunctive

pronouns("sPresente");
echo chr(9);
pronouns("sImperfetto");
echo chr(9);
pronouns("sPassato");
echo chr(9);
pronouns("sTrapassato");
echo chr(9);

// conditional

pronouns("cPresente");
echo chr(9);
pronouns("cPassato");
echo chr(10);

// loop through and get the data
foreach ($files as $verb) {

    //echo "reading " . $verb;
    //echo "<br/>";
    $file_array = file($dir."\\".$verb);

    // the array now contains the lines

    // infinitive
    echo trim($file_array[1]).chr(9);
    // english
    echo trim($file_array[2]).chr(9);

    // regular or irregular
    $reg =  explode(" ",$file_array[3]);
    echo trim($reg[0]).chr(9);
    // verb type
    $type = explode(" ",$file_array[4]);
    echo trim($type[0]).chr(9);

    // iPresente
    echo trim($file_array[15]).chr(9);
    echo trim($file_array[16]).chr(9);
    echo trim($file_array[17]).chr(9);
    echo trim($file_array[18]).chr(9);
    echo trim($file_array[19]).chr(9);
    echo trim($file_array[20]).chr(9);
    // iImperfetto
    echo trim($file_array[29]).chr(9);
    echo trim($file_array[30]).chr(9);
    echo trim($file_array[31]).chr(9);
    echo trim($file_array[32]).chr(9);
    echo trim($file_array[33]).chr(9);
    echo trim($file_array[34]).chr(9);
    // iPassatoRemoto
    echo trim($file_array[43]).chr(9);
    echo trim($file_array[44]).chr(9);
    echo trim($file_array[45]).chr(9);
    echo trim($file_array[46]).chr(9);
    echo trim($file_array[47]).chr(9);
    echo trim($file_array[48]).chr(9);
    // iFuturoSemplice
    echo trim($file_array[57]).chr(9);
    echo trim($file_array[58]).chr(9);
    echo trim($file_array[59]).chr(9);
    echo trim($file_array[60]).chr(9);
    echo trim($file_array[61]).chr(9);
    echo trim($file_array[62]).chr(9);
    // iPassatoProssimo
    echo trim($file_array[72]).chr(9);
    echo trim($file_array[73]).chr(9);
    echo trim($file_array[74]).chr(9);
    echo trim($file_array[75]).chr(9);
    echo trim($file_array[76]).chr(9);
    echo trim($file_array[77]).chr(9);
    // iTrapassatoProssimo
    echo trim($file_array[86]).chr(9);
    echo trim($file_array[87]).chr(9);
    echo trim($file_array[88]).chr(9);
    echo trim($file_array[89]).chr(9);
    echo trim($file_array[90]).chr(9);
    echo trim($file_array[91]).chr(9);
    // iTrapassatoRemoto
    echo trim($file_array[100]).chr(9);
    echo trim($file_array[101]).chr(9);
    echo trim($file_array[102]).chr(9);
    echo trim($file_array[103]).chr(9);
    echo trim($file_array[104]).chr(9);
    echo trim($file_array[105]).chr(9);
    // iFutureAnteriore
    echo trim($file_array[114]).chr(9);
    echo trim($file_array[115]).chr(9);
    echo trim($file_array[116]).chr(9);
    echo trim($file_array[117]).chr(9);
    echo trim($file_array[118]).chr(9);
    echo trim($file_array[119]).chr(9);

    // sPresente
    echo trim($file_array[135]).chr(9);
    echo trim($file_array[136]).chr(9);
    echo trim($file_array[137]).chr(9);
    echo trim($file_array[138]).chr(9);
    echo trim($file_array[139]).chr(9);
    echo trim($file_array[140]).chr(9);
    // sImperfetto
    echo trim($file_array[149]).chr(9);
    echo trim($file_array[150]).chr(9);
    echo trim($file_array[151]).chr(9);
    echo trim($file_array[152]).chr(9);
    echo trim($file_array[153]).chr(9);
    echo trim($file_array[154]).chr(9);
    // sPassato
    echo trim($file_array[164]).chr(9);
    echo trim($file_array[165]).chr(9);
    echo trim($file_array[166]).chr(9);
    echo trim($file_array[167]).chr(9);
    echo trim($file_array[168]).chr(9);
    echo trim($file_array[169]).chr(9);
    // sTrapassato
    echo trim($file_array[178]).chr(9);
    echo trim($file_array[179]).chr(9);
    echo trim($file_array[180]).chr(9);
    echo trim($file_array[181]).chr(9);
    echo trim($file_array[182]).chr(9);
    echo trim($file_array[183]).chr(9);

    // cPresente"
    echo trim($file_array[199]).chr(9);
    echo trim($file_array[200]).chr(9);
    echo trim($file_array[201]).chr(9);
    echo trim($file_array[202]).chr(9);
    echo trim($file_array[203]).chr(9);
    echo trim($file_array[204]).chr(9);
    // cPassato"
    echo trim($file_array[214]).chr(9);
    echo trim($file_array[215]).chr(9);
    echo trim($file_array[216]).chr(9);
    echo trim($file_array[217]).chr(9);
    echo trim($file_array[218]).chr(9);
    echo trim($file_array[219]).chr(9);

    echo chr(10);
}
