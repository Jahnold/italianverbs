/**
 * Created with JetBrains PhpStorm.
 * User: Matthew
 * Date: 13/11/13
 * Time: 22:54
 * To change this template use File | Settings | File Templates.
 */

var $_GET = {};

document.location.search.replace(/\??(?:([^=]+)=([^&]*)&?)/g, function () {
    function decode(s) {
        return decodeURIComponent(s.split("+").join(" "));
    }

    $_GET[decode(arguments[1])] = decode(arguments[2]);
});

// load sound manager
soundManager.setup({

    // where to find the SWF files, if needed
    url: location.pathname +'swf/'

});

function loadVerb(verb) {

    // load a verb from the fusion table and display it's conjugations in the table
    var table = '12w8rxZjebVDDMX8PTb1SfDxuRV4fhH1A1UnU2_4';
    var queryUrlHead = 'https://www.googleapis.com/fusiontables/v1/query?sql=';
    var queryUrlTail = '&jsonCallback=callback&key=AIzaSyDm7dJBMQHXmOK6XZled79_VCrTLrv4QTM';

    // SQL
    var query = "SELECT * FROM " + table + " WHERE Infinito='" + verb + "'";

    var queryurl = encodeURI(queryUrlHead + query + queryUrlTail);

    $.get(queryurl, function (data) {

            // the data var should now hold our json object full of road data
            var verbData = data.rows[0];
            var columns = data.columns;

            // loop through the column headers and add the data to the
            // items in the document which I've cunningly given the matching id
            for (var j = 0; j < columns.length; j++) {

                $('#'+columns[j]).text(verbData[j]);
            }


        }
        , "jsonp");
}

$(document).ready(function () {

    // load the verbs from the fusion table and populate the list
    var table = '12w8rxZjebVDDMX8PTb1SfDxuRV4fhH1A1UnU2_4';
    var queryUrlHead = 'https://www.googleapis.com/fusiontables/v1/query?sql=';
    var queryUrlTail = '&jsonCallback=callback&key=AIzaSyDm7dJBMQHXmOK6XZled79_VCrTLrv4QTM';

    // SQL
    var query = "SELECT 'Infinito' FROM " + table;

    var queryurl = encodeURI(queryUrlHead + query + queryUrlTail);

    $.get(queryurl, function (data) {

            // the data var should now hold our json object full of road data
            var verbs = data.rows;
            var $verblist = $('#verbs');
            var verbarray = [];

            // loop through the list and add the items
            for (var j = 0; j < verbs.length; j++) {

                // append list item
                $verblist.append('<li class="verb" data-verb="' + verbs[j][0] + '">' + verbs[j][0] + '</li>');
                verbarray[j] = verbs[j][0];
            }

            // if there is a verb specified in the GET load that
            if (jQuery.inArray($_GET['verb'],verbarray) != -1) {

                // the verb passed in is recognised, load it
                $("[data-verb='" + $_GET['verb'] + "']").addClass('selected');
                loadVerb($_GET['verb']);
            }
            else {
                // load abbandonare by default
                $('.verb').first().addClass('selected');
                loadVerb('abbandonare');
            }
            // clear the loading image and display main div
            $('#loading').addClass('hidden');
            $('#main-div').removeClass('hidden');

    }
    , "jsonp");

    // user clicks on a verb
    $(document).on('click','.verb',function() {

        // clear current selected highlight
        $('.selected').removeClass('selected');

        var $this = $(this);
        $this.addClass('selected');
        loadVerb($this.text());
    });

    // user hovers over a conjunction, show listen icon
    var $verbword = $('.verbword');
    $verbword.parent('td').mouseenter(function() {
        $(this).children('.icon-listen').removeClass('hidden');
    });
    $verbword.parent('td').mouseleave(function() {
        $(this).children('.icon-listen').addClass('hidden');
    });

    // user clicks on listen icon
    $('.icon-listen').click(function() {

        var text = $(this).siblings('span').text();
        var url = encodeURI('tts.php?words=' + text);

        var sound = soundManager.createSound({
            url: url
        });
        sound.play();
    });

    // user clicks on the clear icon
    $('.icon-clear').click(function() {

        var $search = $('#search');
        // clear the current search text
        $search.val('');
        // trigger a keyup so that the list is refreshed
        $search.trigger('keyup');

    });
});